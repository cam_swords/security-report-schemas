FROM node:14-alpine3.11

ADD . /security-report-schemas
WORKDIR /security-report-schemas
ENV NODE_PATH=/usr/local/lib/node_modules
ARG BASH_UNIT_SHA1SUM=f96b8dfa01f953c366a10168b97d7f901e1d6f32

RUN apk add --no-cache jq curl bash && \
      curl -s -O https://raw.githubusercontent.com/pgrange/bash_unit/v1.7.1/install.sh && \
      echo "${BASH_UNIT_SHA1SUM}  install.sh" | sha1sum -c && \
      bash <install.sh && \
      rm install.sh && \
      mv ./bash_unit /usr/local/bin && \
      npm install -g && \
      npm install -g esm && \
      ./scripts/distribute.sh
