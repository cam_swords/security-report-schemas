import {merge} from '../../support/merge'

const defaults = {
  end_time: '2020-12-04T03:26:19',
  messages: [],
  scanned_resources: [
    {
      method: 'GET',
      type: 'url',
      url: 'http://nginx'
    }
  ],
  scanner: {
    id: 'zaproxy',
    name: 'OWASP Zed Attack Proxy (ZAP)',
    url: 'https://www.zaproxy.org',
    version: 'D-2020-06-30',
    vendor: {
      name: 'GitLab'
    }
  },
  start_time: '2020-12-04T03:26:07',
  status: 'success',
  type: 'dast'
}

export const scan = (scanOverrides) => {
    return merge(defaults, scanOverrides)
}
