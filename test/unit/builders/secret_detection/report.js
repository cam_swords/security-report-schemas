import {merge} from '../../support/merge'
import {scan} from './scan'
import {vulnerability} from './vulnerability'

const defaults = {
  remediations: [],
  scan: scan(),
  version: '100000.0.0',
  vulnerabilities: [vulnerability()]
}

export const report = (reportOverrides) => {
  return merge(defaults, reportOverrides)
}
