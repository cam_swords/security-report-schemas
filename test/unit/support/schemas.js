import {Validator} from 'jsonschema'
import * as dastSchema from '../../../dist/dast-report-format.json'
import * as sastSchema from '../../../dist/sast-report-format.json'
import * as dependencyScanningSchema from '../../../dist/dependency-scanning-report-format.json'
import * as secretDetectionSchema from '../../../dist/secret-detection-report-format.json'
import * as containerScanningSchema from '../../../dist/container-scanning-report-format.json'
import * as coverageFuzzingSchema from '../../../dist/coverage-fuzzing-report-format.json'
import * as jsonDraft07Specification from './json-schema-draft-07.json'

const validateSchema = (schema) => {
  return (report) => {
    const result = new Validator().validate(report, schema)

    return {
      success: result.errors.length === 0,
      errors: result.errors.join('; ')
    }
  }
}

export const schemas = {
  dast: {validate: validateSchema(dastSchema)},
  sast: {validate: validateSchema(sastSchema)},
  dependency_scanning: {validate: validateSchema(dependencyScanningSchema)},
  secret_detection: {validate: validateSchema(secretDetectionSchema)},
  container_scanning: {validate: validateSchema(containerScanningSchema)},
  coverage_fuzzing: {validate: validateSchema(coverageFuzzingSchema)},
  json_draft_07: {validate: validateSchema(jsonDraft07Specification)},
}
