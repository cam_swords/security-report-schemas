import b from './builders/index'
import {schemas} from './support/schemas'
import {withVulnerabilityDetails} from './builders/vulnerability_details/vulnerability'

const schemaTypes = [
  {name: 'dast', builder: b.dast, schema: schemas.dast},
  {name: 'sast', builder: b.sast, schema: schemas.sast},
  {name: 'container_scanning', builder: b.container_scanning, schema: schemas.container_scanning},
  {name: 'coverage_fuzzing', builder: b.coverage_fuzzing, schema: schemas.coverage_fuzzing},
  {name: 'secret_detection', builder: b.secret_detection, schema: schemas.secret_detection},
  {name: 'dependency_scanning', builder: b.dependency_scanning, schema: schemas.dependency_scanning}
]


schemaTypes.forEach((type) => {
  describe(`common[${type.name}] schema`, () => {
    it('should not validate when there are no identifiers', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: []
        })]
      })

      expect(type.schema.validate(report).errors).toContain('identifiers does not meet minimum length of 1')
    })

    it('should not validate with empty identifier[].type', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: [{
            type: "",
            name: "name",
            value: "value"
          }]
        })]
      })

      expect(type.schema.validate(report).errors).toContain('instance.vulnerabilities[0].identifiers[0].type does not meet minimum length of 1')
    })

    it('should not validate with empty identifier[].name', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: [{
            type: "type",
            name: "",
            value: "value"
          }]
        })]
      })

      expect(type.schema.validate(report).errors).toContain('instance.vulnerabilities[0].identifiers[0].name does not meet minimum length of 1')
    })

    it('should not validate with empty identifier[].value', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          identifiers: [{
            type: "type",
            name: "name",
            value: ""
          }]
        })]
      })

      expect(type.schema.validate(report).errors).toContain('instance.vulnerabilities[0].identifiers[0].value does not meet minimum length of 1')
    })

    it('must have a version', () => {
      const report = type.builder.report({version: undefined})
      expect(type.schema.validate(report).errors).toContain('requires property "version"')
    })

    it('can have zero vulnerabilities', () => {
      const report = type.builder.report({vulnerabilities: []})
      expect(type.schema.validate(report).success).toBeTruthy()
    })

    it('validates with additional details about vulnerabilities', () => {
      const report = withVulnerabilityDetails(type.builder.report());
      expect(type.schema.validate(report).success).toBeTruthy();
    });

    it('validates source tracking type', () => {
      const report = type.builder.report({
        vulnerabilities: [type.builder.vulnerability({
          tracking: {
            type: "source",
            items: [{
              file: "app/controllers/groups_controller.rb",
              start_line: 6,
              end_line: 6,
              signatures: [{
                "algorithm": "scope_offset",
                "value": "app/controllers/groups_controller.rb|GroupsController[0]|new_group[0]:4"
              }]
            }]
          }
        })]
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })

    it('complies to the JSON specification', () => {
      const report = type.builder.report()
      expect(schemas.json_draft_07.validate(report).success).toBeTruthy()
    })

    it('validates scan analyzer', () => {
      const report = type.builder.report({
        scan: type.builder.scan({
          analyzer: {
            id: 'gitlab-dast',
            name: 'GitLab DAST',
            url: 'https://gitlab.com/dast',
            version: '1.6.20',
            vendor: {
              name: 'GitLab'
            }
          }
        })
      })

      expect(type.schema.validate(report).success).toBeTruthy();
    })
  })
})
