# Security report schemas changelog

## v14.0.2
- Add `scan.analyzer.id` field (!74)
- Add `scan.analyzer.name` field (!74)
- Add `scan.analyzer.url` field (!74)
- Add `scan.analyzer.version` field (!74)
- Add `scan.analyzer.vendor.name` field (!74)

## v14.0.1
- Added `vulnerabilities[].tracking` field (!69)

## v14.0.0
- Add `minLength` requirement to `identifiers` required fields (!71)
- Add `minLength` requirement to `identifiers[].type` field (!71)
- Add `minLength` requirement to `identifiers[].name` field (!71)
- Add `minLength` requirement to `identifiers[].value` field (!71)

## v13.1.0
- Removed `minLength` requirement for the `vulnerabilities[].evidence.response.reason_phrase` field in the DAST schema (!68)
- Removed `minLength` requirement for the `vulnerabilities[].evidence.supporting_messages[].response.reason_phrase` field in the DAST schema (!68)

## v13.0.0
- Added `vulnerabilities.details` to all schemas (!63)

## v12.1.0
- Removed `minLength` requirement for `body` fields in the DAST schema (!64)

## v12.0.0
- Schemas are restricted to having a `scan.type` appropriate to the type of schema (!62)
- Added `scan.type` `secret_detection` (!62)
- Added `scan.type` `api_fuzzing` (!62)

## v11.0.0
- Update string requirements to enforce a minimum length greater than 0 (!56)

## v10.0.0
- Update the `version` pattern to match the naming convention of schema versions (!57)

## v9.0.0
- Add `vulnerabilities[].discovered_at` field to the DAST schema to describe when a vulnerability was discovered (!58)

## v8.1.0
- Remove `vulnerabilities[].target` field from coverage fuzzing schema as it's no longer used (!53)

## v8.0.1
- Add `vulnerabilities[].raw_source_code_extract` field to SAST and Secret-Detection schemas (!52)

## v8.0.0
- Converted `scan.messages[]` to be an optional field (!51)

## v7.0.1
- Add `coverage_fuzzing` to `scan.type` enum (!50)

## v7.0.0
- Removed `vulnerabilities[].stacktrace` field from the Coverage Guided Fuzz Testing schema (!47)
- Added `vulnerabilities[].location.stacktrace_snippet` field to the Coverage Guided Fuzz Testing schema (!47)

## v6.1.0
- Update `vulnerabilities.assets[].type` field from array to enum for the DAST schema (!49)

## v6.0.1
- Update `vulnerabilities.identifier.name` field description to indicate its to be human readable (!48)
- Update `scanner.name` field description to indicate its to be human readable (!48)

## v6.0.0
- Add `vulnerabilities.assets[]` field to the DAST schema to describe the assets associated with a vulnerability (!39)
- Add `vulnerabilities.assets[].type` field to the DAST schema to describe the type of an asset associated with a vulnerability (!39)
- Add `vulnerabilities.assets[].name` field to the DAST schema to describe the name of an asset associated with a vulnerability (!39)
- Add `vulnerabilities.assets[].url` field to the DAST schema to describe the url to an asset associated with a vulnerability (!39)

## v5.0.1
- Add `iid`, `direct`, and `dependency_path` fields to `dependency` object used by Dependency Scanning, Container Scanning schemas (!46)

## v5.0.0
- Add `vulnerabilities[].evidence.source` field to the DAST schema to describe the source of the vulnerability evidence (!40)
- Add `vulnerabilities[].evidence.source.id` field to the DAST schema to describe the identifier of the source of the vulnerability evidence (!40)
- Add `vulnerabilities[].evidence.source.name` field to the DAST schema to describe the name of the source of the vulnerability evidence (!40)
- Add `vulnerabilities[].evidence.source.url` field to the DAST schema to describe the link to additional information about the source of the vulnerability evidence (!40)

## v4.0.0
- Add `vulnerabilities[].evidence.request.body` field to the DAST schema to describe the body of the request (!37)
- Add `vulnerabilities[].evidence.response.body` field to the DAST schema to describe the body of the response (!37)
- Add `vulnerabilities[].evidence.supporting_messages[]` field to the DAST schema to describe supporting HTTP messages (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].name` field to the DAST schema to describe the name of the supporting message (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].request` field to the DAST schema to describe the HTTP request portion of the supporting message (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].request.headers[]` field to the DAST schema to describe the HTTP headers of the supporting request (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].request.method` field to the DAST schema to describe the HTTP method of the supporting request (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].request.url` field to the DAST schema to describe the HTTP URL of the supporting request (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].request.body` field to the DAST schema to describe the body of the supporting request (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].response` field to the DAST schema to describe the HTTP response portion of the supporting message (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].response.headers[]` field to the DAST schema to describe the HTTP headers of the supporting response (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].response.status_code` field to the DAST schema to describe the HTTP status code of the supporting response (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].response.reason_phrase` field to the DAST schema to describe the HTTP reason phrase of the supporting response (!37)
- Add `vulnerabilities[].evidence.supporting_messages[].response.body` field to the DAST schema to describe the body of the supporting response (!37)

## v3.1.0
- Adds `coverage-fuzzing-report-format` schema for Coverage Guided Fuzz Testing report types

## v3.0.1
- Explicitly support `additionalProperties` (!34)

## v3.0.0
- Add `scan.scanner.vendor` field to all schemas to capture the vendor/maintainer of the scanner (!33)

## v2.4.1
- Update descriptions for node attributes (!30)

## v2.4.0
- Define the schema used for secret detection reports

## v2.3.3
- Add `vulnerabilities[].evidence` field to the DAST schema to describe the context of the vulnerability (!14)
- Add `vulnerabilities[].evidence.summary` field to the DAST schema to summarize vulnerability evidence (!14)
- Add `vulnerabilities[].evidence.request` field to the DAST schema to describe information about the HTTP request that resulted in a vulnerability (!14)
- Add `vulnerabilities[].evidence.request.headers[]` field to the DAST schema to describe the HTTP headers of the vulnerable request (!14)
- Add `vulnerabilities[].evidence.request.method` field to the DAST schema to describe the HTTP method of the vulnerable request (!14)
- Add `vulnerabilities[].evidence.request.url` field to the DAST schema to describe the HTTP URL of the vulnerable request (!14)
- Add `vulnerabilities[].evidence.response` field to the DAST schema to describe information about the HTTP response that resulted in a vulnerability (!14)
- Add `vulnerabilities[].evidence.response.headers[]` field to the DAST schema to describe the HTTP headers of the vulnerable response (!14)
- Add `vulnerabilities[].evidence.response.status_code` field to the DAST schema to describe the HTTP status code of the vulnerable response (!14)
- Add `vulnerabilities[].evidence.response.reason_phrase` field to the DAST schema to describe the HTTP reason phrase of the vulnerable response (!14)
- Add `scan.scanned_resources[]` field to the DAST schema to describe the website attack surface (!28)

## v2.3.2
- Add `scan` field to all schemas to describe the context of discovered vulnerabilities (!13)
- Add `scan.messages` field to all schemas to describe human readable information outputted by the scan (!13)
- Add `scan.scanner` field to all schemas to describe information about the scanner that performed the scan (!13)
- Add `scan.start_time` field to all schemas to describe the time the scan started (!13)
- Add `scan.end_time` field to all schemas to describe the time the scan finished (!13)
- Add `scan.status` field to all schemas to describe the result of the scan (!13)
- Add `scan.type` field to all schemas to capture the result of the scan (!13)

## v2.3.1
- Add `vulnerabilities[].id` to all schemas to allow vulnerabilities to be uniquely identified (!11)
- Add `vulnerabilities[].location` to the DAST schema to describe the URL where a vulnerability was found (!17)

## v2.3.0
- Initial commit of the security-report-schemas project matching report version outputted by the GitLab Security Common library (!8)
