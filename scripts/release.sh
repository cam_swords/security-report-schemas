#!/usr/bin/env bash

set -e

if [ "$CI" != true ]; then
  printf >&2 "Error: script is expected to be run from CI Pipeline only.\n"
  exit 1
fi

if [[ -z "$GITLAB_API_TOKEN" ]]; then
  printf "Aborting, environment variable GITLAB_API_TOKEN must contain a GitLab private token with access to this project.\n"
  exit 1
fi

log() {
  printf "\n\n######### %s #########\n" "$*" >>/dev/stdout
}

log "Initializing environment"
WORKING_DIRECTORY="$(dirname "$(realpath "$0")")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$WORKING_DIRECTORY/changelog-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$WORKING_DIRECTORY/release-utils.sh"

PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/security-report-schemas"}

VERSION="$(changelog_last_version)"
CHANGELOG_DESCRIPTION=$(changelog_last_description)
RELEASE_DATA=$(build_release_json_payload "$VERSION" "$CHANGELOG_DESCRIPTION" "$PROJECT_URL")

log "Detected Secure Report Format $VERSION, verifying not already released"
verify_version_not_released "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$VERSION"

log "Tagging Git SHA $CI_COMMIT_SHA with $VERSION"
tag_git_commit "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$VERSION" "$CI_COMMIT_SHA"

log "Creating GitLab release from Git tag $VERSION"
create_gitlab_release "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$RELEASE_DATA"
