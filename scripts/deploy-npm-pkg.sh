#!/usr/bin/env bash

set -e

if [ "$CI" != true ]; then
  printf >&2 "Error: script is expected to be run from CI Pipeline only.\n"
  exit 1
fi

log() {
  printf "\n\n######### %s #########\n" "$*" >>/dev/stdout
}

# publish_schemas_to_registry generates a schemas-only package to be published
# to specified npm package registries.
# arguments: [Registry URL] [Repository Auth Token] [Version] [Project URL] [PKG Scope]
publish_schemas_to_registry() {
  local registry="${1#https*://}" # remove protocol
  local registry_token="$2"
  local pkg_version="${3#v}"
  # shellcheck disable=SC2034 # (unused var), used by envsubst in template
  local project_url="$4"
  local npm_pkg_scope="@$5"
  local pkgdir="npmpkg"

  pushd "$PROJECT_DIR" >/dev/null

  # Build package to publish
  mkdir -p "$pkgdir/dist"
  cp dist/*.json "$pkgdir/dist/"
  cp LICENSE.md "$pkgdir"
  cp CHANGELOG.md "$pkgdir"
  cp src/npmpkg-template/* "$pkgdir"

  # Fill in template files with variable replacements
  # Note: prevent env pollution by calling from a subshell
  resolveTemplate() {
    local targetFile="$1"
    local inputFile="src/npmpkg-template"
    inputFile="$inputFile/$(basename "$targetFile")"
    export pkg_version project_url npm_pkg_scope
    envsubst <"$inputFile" >"$targetFile"
  }
  local templates=("$pkgdir/package.json" "$pkgdir/README.md")
  local t
  for t in "${templates[@]}"; do
    if ! (resolveTemplate "$t"); then
      echo >&2 "Error: failed to resolve template '$t'."
      exit 1
    fi
  done
  echo "Built $npm_pkg_scope/security-report-schemas@$pkg_version"

  pushd "$pkgdir" >/dev/null
  echo "Publishing to https://$registry"
  npm config set --userconfig .npmrc "$npm_pkg_scope:registry" "https://$registry"    # set scope to location
  npm config set --userconfig .npmrc "//${registry%%/}/:_authToken" "$registry_token" # set auth token to registry
  npm publish
  popd >/dev/null

  # cleanup
  rm -r "$pkgdir"
  popd >/dev/null
}

log "Initializing environment"
WORKING_DIRECTORY="$(dirname "$(realpath "$0")")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$WORKING_DIRECTORY/changelog-utils.sh"

PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/security-report-schemas"}
VERSION="$(changelog_last_version)"
REGISTRY_URL="https://$CI_SERVER_HOST/api/v4/projects/$CI_PROJECT_ID/packages/npm/"

log "Publishing package of $VERSION schemas to Project Package Registry on GitLab"
publish_schemas_to_registry "$REGISTRY_URL" "$CI_JOB_TOKEN" "$VERSION" "$PROJECT_URL" "$CI_PROJECT_ROOT_NAMESPACE"
